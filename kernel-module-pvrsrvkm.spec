%define kernel_release 6.6.3-300.ti.fc39.aarch64
%define kernel_version 6.6.3
%define distlocalversion 300.ti

%define	kmod_name pvrsrvkm

%global debug_package %{nil}

%global gitdate 20231109
%global pvrversion 23.2.6460340
%global commitid ba7b23dc
%global platform j784s4

# Use this variable to bump the package revision
%global bumprev 2

Name:          kernel-module-%{kmod_name}-%{platform}
Version:       %{kernel_version}
Release:       %{distlocalversion}.%{bumprev}.%{gitdate}git%{commitid}%{?dist}

Group:         System Environment/Kernel
Summary:       TI Imagination Technologies display driver kernel module

License:       GPL-2.0 OR MIT
URL:           https://git.ti.com/cgit/graphics/ti-img-rogue-driver/
BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0:       ti-img-rogue-driver-%{gitdate}git%{commitid}.tar.xz
Patch0:        rgxdebug-workaround-write-beyond-size-of-object.patch
Patch1:        rogue-km-gpu-cache-coherency.patch

BuildRequires:	kernel-devel = %{kernel_version}
Requires:	kernel-core = %{kernel_version}

ExclusiveArch: aarch64

%description
The TI Imagination Technologies Rogue DDK %{pvrversion} display driver kernel module for kernel %{kernel_version}.

%prep
%setup -q -n ti-img-rogue-driver-%{gitdate}git%{commitid}
# apply patches and do other stuff here
%patch -P0 -p1

%build
pushd build/linux/%{platform}_linux
KERNELDIR=/usr/src/kernels/%{kernel_release}/ make W=1 BUILD=release
popd

%install
rm -rf ${RPM_BUILD_ROOT}
mkdir -p ${RPM_BUILD_ROOT}/lib/modules/%{kernel_release}/extra/
install -D -m 755 ./binary_%{platform}_linux_wayland_release/target_aarch64/pvrsrvkm.ko ${RPM_BUILD_ROOT}/lib/modules/%{kernel_release}/extra/

%post
depmod

%clean
rm -rf ${RPM_BUILD_ROOT}

%files
/lib/modules/%{kernel_release}/extra/pvrsrvkm.ko

%changelog
* Wed Nov 29 2023 Enric Balletbo i Serra <eballetbo@redhat.com> - 23.2.6460340-2
- Added IMG's patch for the HWRs we see on AM69

* Fri Nov 3 2023 Enric Balletbo i Serra <eballetbo@redhat.com> - 23.2.6460340-1
- Initial RPM release for j784s4 platform
